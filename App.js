import React, { useState } from 'react';
import { Text, View, TextInput, TouchableOpacity } from 'react-native';

function App(){
  const [inputText, setInput] = useState("")
  const [key] = useState("Prog")
  const [result, setResult] = useState("")

  function findString(input, key){
    let keyword = key
    if(input.length < 3){
      alert("Minimal input 3 huruf")
    }
    else{
      let keyWordFound = false
      let keyNum = 0
      let keywordSplit = keyword.toLowerCase().split(" ").join("").split("")
      let inputSplit = input.toLowerCase().split(" ")
      if(input.length >= keyword.length){
        for(let split = 0; split < inputSplit.length; split++){
          let words = inputSplit[split].split("")
          for(let arr = 0; arr < words.length; arr++){
            if(words[arr] === keywordSplit[keyNum]){
              keyNum++
              if(keyNum === keywordSplit.length){
                keyWordFound = true
                arr = words.length
                split = inputSplit.length
              }        
            }
            else{
              keyWordFound = false
              keyNum = 0
            }
          }
          keyNum = 0
        }
        if(keyWordFound){
          setResult(input)
        }
        else{
          setResult("")
  
        }
      }
      else{
        setResult("")
      }
    }
  }

  return (
    <View style={{flex:1, justifyContent:'center'}}>
      <TextInput style={{
        borderWidth:2, 
        borderColor:'black',
        height:50,
        paddingHorizontal: 20,
        marginHorizontal: 20,
        fontSize:18
        }}
        onChangeText={(e)=>{
          setInput(e)
          setResult("")
        }}
        placeholder={"Masukkan Teks disini..."}
        />
        <TouchableOpacity onPress={()=>{findString(inputText, key)}}>
          <View style={{
            backgroundColor:'green', 
            width:100, 
            height: 40, 
            alignItems:'center', 
            justifyContent:'center',
            marginTop:20,
            marginLeft: 20,
            borderRadius:10
            }}>
            <Text style={{
              color:'white'
            }}>Find String</Text>
          </View>
        </TouchableOpacity>
        <View style={{marginLeft:20, marginTop:20}}>
          <Text>
            Result:
          </Text>
          <Text>
            {result}
          </Text>
        </View>
        
    </View>
  );
}

export default App;